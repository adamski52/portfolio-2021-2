import { makeJob } from "../setupTests";
import FilteringService from "./FilteringService";
import JobsService, { IJob } from "./JobsService";

const job1:IJob = makeJob("Company 1", ["javascript", "kafka", "html"], new Date(2018, 0, 1).getTime(), new Date(2019, 0, 1).getTime()),
      job2:IJob = makeJob("Company 2", ["typescript", "photoshop", "illustrator"], new Date(2019, 0, 1).getTime(), new Date(2020, 0, 1).getTime()),
      job3:IJob = makeJob("Job 3", ["flash", "actionscript", "camel"], new Date(2020, 0, 1).getTime(), undefined);

let jobsService:JobsService;
let filteringService:FilteringService;
describe("JobsService", () => {
    beforeEach(() => {
        filteringService = new FilteringService();
        jobsService = new JobsService(filteringService, [job1, job2, job3]);
    });

    it("should get all jobs if no filter", () => {
        let actual = jobsService.getJobs();
        expect(actual.length).toEqual(3);
    });

    it("should get all jobs if filter length <=3", () => {
        let actual = jobsService.getJobs("lol");
        expect(actual.length).toEqual(3);
    });

    it("should get all jobs whose companyName closely matches", () => {
        let actual = jobsService.getJobs("company");
        expect(actual.length).toEqual(2);

        actual = jobsService.getJobs("Job 3");
        expect(actual.length).toEqual(1);
    });

    it("should get all jobs whose jobTitle closely matches", () => {
        let actual = jobsService.getJobs("Company jobTitle");
        expect(actual.length).toEqual(2);

        actual = jobsService.getJobs("Job jobTitle");
        expect(actual.length).toEqual(1);
    });

    it("should get jobs exactly matching at least 1 filter", () => {
        let actual = jobsService.getJobs("javascript");
        expect(actual.length).toEqual(1);
    });

    it("should get jobs fuzzy matching at least 1 filter", () => {
        let actual = jobsService.getJobs("javascri");
        expect(actual.length).toEqual(1);
    });

    it("should not get jobs that are too far off a match", () => {
        let actual = jobsService.getJobs("kafkazzzzzz");
        expect(actual.length).toEqual(0);
    });

    it("should return false if item is not defined", () => {
        let actual = jobsService.isSkillActive(null, "whatever");
        expect(actual).toEqual(false);
    });

    it("should return false if item does not have skill", () => {
        let actual = jobsService.isSkillActive(job1, "skill4");
        expect(actual).toEqual(false);
    });

    it("should return true if item does have skill", () => {
        let actual = jobsService.isSkillActive(job1, "javascript");
        expect(actual).toEqual(true);
    });
});
