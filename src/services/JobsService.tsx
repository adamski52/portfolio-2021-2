import React, { RefObject } from "react";
import ITaggedItem from "../interfaces/TaggedItem";
import FilteringService from "./FilteringService";

export interface IJob extends ITaggedItem {
    companyName: string;
    jobTitle: string;
    description: string[];
    startDate: number;
    endDate?: number;
    thumbnailUrl?: string;
    themeColor: string;
}

export interface IJobRef extends IJob {
    ref: RefObject<any>;
}

export default class JobsService {
    private jobs:IJobRef[];
    private matchThreshold = .6;
    private filteringService:FilteringService;

    constructor(filteringService:FilteringService, jobs:IJob[]) {
        this.jobs = jobs.map((item) => {
            return {
                ...item,
                ref: React.createRef()
            };
        });

        this.filteringService = filteringService;
    }

    public getJobs(filter: string = "") {
        if (!filter || filter.length <= 3) {
            return this.jobs;
        }

        filter = filter.toLowerCase();

        return this.jobs.filter((job) => {
            let companyName = job.companyName.toLowerCase();
            if(companyName.indexOf(filter) > -1 || this.filteringService.getSimilarity(companyName, filter) > this.matchThreshold) {
                return true;
            }
            
            let jobTitle = job.jobTitle.toLowerCase();
            if(jobTitle.indexOf(filter) > -1 || this.filteringService.getSimilarity(jobTitle, filter) > this.matchThreshold) {
                return true;
            }

            for (let i = 0; i < job.tags.length; i++) {
                let tag = job.tags[i].toLowerCase();
                if (tag.indexOf(filter) > -1 || this.filteringService.getSimilarity(tag, filter) > this.matchThreshold) {
                    return true;
                }
            }
            return false;
        });
    }

    public get allTags() {
        let tags:string[][] = [];
        this.jobs.forEach((job) => {
            tags.push(job.tags);
        });

        return tags;
    }

    public isSkillActive(job:IJob | null, skillName:string) {
        if(!job) {
            return false;
        }

        return job.tags.indexOf(skillName.toLowerCase()) > -1;
    }

    private months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    private toReadableDate(date:Date) {
        return this.months[date.getMonth()] + " " + date.getFullYear();
    }

    public getJobDateDisplay(job:IJob) {
        let startDate = this.toReadableDate(new Date(job.startDate)),
            endDate = job.endDate ? this.toReadableDate(new Date(job.endDate)) : "Present";

        return startDate + " - " + endDate;
    }
}