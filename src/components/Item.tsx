import React, { MouseEvent } from "react";
import { IItemRef } from "../services/ItemsService";
import "./Item.scss";

export interface IItemProps {
    item: IItemRef;
    gotoUrl: (url: string) => void;
    isActive: boolean;
}

export interface IItemState {
    item: IItemRef;
    isActive: boolean;
}

export default class Item extends React.Component<IItemProps, IItemState> {
    constructor(props:IItemProps) {
        super(props);

        this.state = {
            ...props
        };

        this.onViewCode = this.onViewCode.bind(this);
        this.onViewLive = this.onViewLive.bind(this);
    }
    
    private gotoUrl(url:string | undefined) {
        if(!url) {
            return;
        }

        this.props.gotoUrl(url);
    }

    private onViewCode(e:MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        
        this.gotoUrl(this.state.item.codeUrl);
    }

    private onViewLive(e:MouseEvent<HTMLButtonElement>) {
        e.preventDefault();

        this.gotoUrl(this.state.item.liveUrl);
    }

    private renderDescription() {
        return this.state.item.description.map((text, index) => {
            return (
                <p key={index}>{text}</p>
            );
        });
    }

    private renderThumbnail() {
        if(!this.state.item.thumbnailUrl) {
            return null;
        }

        return (
            <img className="item-thumbnail" src={this.state.item.thumbnailUrl} alt={this.state.item.name} />
        );
    }
    
    private renderButton(url:string | undefined, text:string, className: string, method:(e:MouseEvent<HTMLButtonElement>) => void) {
        if(!url) {
            return null;
        }

        return (
            <button onClick={method} className={"btn " + className} >{text}</button>
        );
    }

    private getClassName() {
        let classes = ["item"];

        if(this.state.isActive) {
            classes.push("active");
        }

        return classes.join(" ");
    }

    public render() {
        return (
            <div className={this.getClassName()} ref={this.state.item.ref}>
                <h2 className="item-name">{this.state.item.name}</h2>
                <h3 className="item-summary">{this.state.item.summary}</h3>
                {this.renderThumbnail()}
                <div className="item-description">
                    {this.renderDescription()}
                </div>
                <div className="row item-buttons">
                    <div className="col-6">
                        {this.renderButton(this.state.item.codeUrl, "View Code", "btn-code", this.onViewCode)}
                    </div>
                    <div className="col-6">
                        {this.renderButton(this.state.item.liveUrl, "View Live", "btn-live", this.onViewLive)}
                    </div>
                </div>
            </div>
        );
    }

    public static getDerivedStateFromProps(props:IItemProps, state:IItemState) {
        return {
            ...props
        };
    }
}