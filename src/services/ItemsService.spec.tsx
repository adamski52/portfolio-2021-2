import { makeItem } from "../setupTests";
import FilteringService from "./FilteringService";
import ItemsService, { IItem } from "./ItemsService";

const item1:IItem = makeItem("portfolio (2018)", ["javascript", "kafka", "html"]),
      item2:IItem = makeItem("PorTFolIo (2020)", ["typescript", "css", "scss"]),
      item3:IItem = makeItem("I'm a project", ["photoshop", "illustrator", "actionscript"]);

let itemsService:ItemsService;
let filteringService:FilteringService;
describe("ItemsService", () => {
    beforeEach(() => {
        filteringService = new FilteringService();
        itemsService = new ItemsService(filteringService, [item1, item2, item3]);
    });

    it("should get all items if no filter", () => {
        let actual = itemsService.getItems();
        expect(actual.length).toEqual(3);
    });

    it("should get all items if filter length <=3", () => {
        let actual = itemsService.getItems("lol");
        expect(actual.length).toEqual(3);
    });

    it("should get all items whose name closely matches", () => {
        let actual = itemsService.getItems("portfolio");
        expect(actual.length).toEqual(2);

        actual = itemsService.getItems("Im a project");
        expect(actual.length).toEqual(1);
    });

    it("should get items exactly matching at least 1 filter", () => {
        let actual = itemsService.getItems("javascript");
        expect(actual.length).toEqual(1);
    });

    it("should get items fuzzy matching at least 1 filter", () => {
        let actual = itemsService.getItems("javascri");
        expect(actual.length).toEqual(1);
    });

    it("should not get items that are too far off a match", () => {
        let actual = itemsService.getItems("kafkazzzzzz");
        expect(actual.length).toEqual(0);
    });

    it("should return false if item is not defined", () => {
        let actual = itemsService.isSkillActive(null, "whatever");
        expect(actual).toEqual(false);
    });

    it("should return false if item does not have skill", () => {
        let activeItem = makeItem("item", ["skill1", "skill2", "skill3"]);
        let actual = itemsService.isSkillActive(activeItem, "skill4");
        expect(actual).toEqual(false);
    });

    it("should return true if item does have skill", () => {
        let activeItem = makeItem("item", ["skill1", "skill2", "skill3"]);
        let actual = itemsService.isSkillActive(activeItem, "skill1");
        expect(actual).toEqual(true);
    });
});
