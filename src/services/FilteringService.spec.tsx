import FilteringService from "./FilteringService";

describe("FilteringService", () => {
    const filteringService = new FilteringService();

    it("should gimme percent match", () => {
        expect(filteringService.getSimilarity("foo", "bar")).toEqual(0);
        expect(filteringService.getSimilarity("this", "that")).toEqual(.5);
        expect(filteringService.getSimilarity("candybar", "randycar")).toEqual(.75);
        expect(filteringService.getSimilarity("1234567890", "123456789X")).toEqual(.9);
        expect(filteringService.getSimilarity("lolwat", "lolwat")).toEqual(1);
    });
});