import { render, RenderResult } from "@testing-library/react"
import Nav from "./Nav";

describe("Nav", () => {
    let component:RenderResult;
    let onItemClick = jest.fn();
    let navItems = [{
        isActive: false,
        key: "lol",
        text: "LOL"
    }, {
        isActive: true,
        key: "wat",
        text: "WAT"
    }, {
        isActive: false,
        key: "lolwat",
        text: "who dat"
    }];
    
    it("should have all nav items", () => {
        component = render(<Nav items={navItems} onItemClick={onItemClick} />);

        expect(component.queryByText("LOL")).toBeInTheDocument();
        expect(component.queryByText("WAT")).toBeInTheDocument();
        expect(component.queryByText("who dat")).toBeInTheDocument();
    });

    it("should not have active class if not active", () => {
        component = render(<Nav items={navItems} onItemClick={onItemClick} />);

        expect(component.queryByText("LOL")).not.toHaveClass("active");
        expect(component.queryByText("WAT")).toHaveClass("active");
        expect(component.queryByText("who dat")).not.toHaveClass("active");
    });

    it("should call callback on click", () => {
        component = render(<Nav items={navItems} onItemClick={onItemClick} />);
        navItems.forEach((navItem) => {
            component.queryByText(navItem.text)?.click();
            expect(onItemClick).toHaveBeenCalledWith(navItem);
        });
        expect(onItemClick).toHaveBeenCalledTimes(navItems.length);
    });
});
