// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';

export function makeItem(slug:string, tags: string[]) {
    return {
        "name": slug,
        "liveUrl": "http://" + slug + ".com/live",
        "codeUrl": "https://" + slug + ".com/code",
        "thumbnailUrl": "http://" + slug + ".com/thumbnail",
        "summary": "Summary " + slug,
        "awardsUrls": [{
            "where": slug + " where",
            "url": "http://" + slug + ".com/awards"
        }],
        "description": [
            "I'm " + slug,
            "You are not " + slug
        ],
        "tags": tags,
        "themeColor": "#000"
    };
}

export function makeJob(slug:string, tags: string[], startDate: number, endDate: number | undefined) {
    return {
        "companyName": slug,
        "jobTitle": slug + "-jobTitle",
        "thumbnailUrl": "http://" + slug + ".com/thumbnail",
        "description": [
            "I'm " + slug,
            "You are not " + slug
        ],
        "tags": tags,
        "themeColor": "#000",
        "startDate": startDate,
        "endDate": endDate
    };
}
