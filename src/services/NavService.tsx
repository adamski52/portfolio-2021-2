export interface INavItem {
    text: string;
    key: string;
    isActive: boolean;
}

export const NAV_ITEMS = {
    PROJECTS: "projects",
    JOBS: "jobs",
    RESUME: "resume"
}

export default class NavService {
    private items:INavItem[] = [{
        text: "Projects",
        key: NAV_ITEMS.PROJECTS,
        isActive: true
    }, {
        text: "History",
        key: NAV_ITEMS.JOBS,
        isActive: false
    }, {
        text: "Resume",
        key: NAV_ITEMS.RESUME,
        isActive: false
    }];

    public getNavItems():INavItem[] {
        return this.items;
    }

    public setActiveItem(key:string) {
        this.items = this.items.map((item) => {
            if(item.key === key) {
                return {
                    ...item,
                    isActive: true
                };
            }

            return {
                ...item,
                isActive: false
            };
        });
    }

    public getActiveItem():INavItem {
        let activeItem = this.items.find((item) => {
            return item.isActive;
        });

        if(activeItem) {
            return activeItem;
        }

        this.setActiveItem(this.items[0].key);
        return this.items[0];
    }
}