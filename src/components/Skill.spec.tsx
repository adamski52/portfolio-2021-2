import React from "react"
import { render, RenderResult } from "@testing-library/react"
import Skill from "./Skill";
import { ISkill } from "../services/SkillsService";

describe("Skill", () => {
    let component:RenderResult;

    const skill:ISkill = {
        name: "skill-name",
        skill: 42
    };

    it("should render", () => {
        component = render(<Skill isActive={false} skill={skill} />);
    });

    it("should have the skill name", () => {
        component = render(<Skill isActive={false} skill={skill} />);
        expect(component.queryByAltText("skill-name")).toBeInTheDocument();
    });

    it("should not have active class if not active", () => {
        component = render(<Skill isActive={false} skill={skill} />);
        expect(component.container.firstChild).not.toHaveClass("active");
    });

    it("should have active class if active", () => {
        component = render(<Skill isActive={true} skill={skill} />);
        expect(component.container.firstChild).toHaveClass("active");
    });
});
