import React from "react"
import { render, RenderResult } from "@testing-library/react"
import { makeJob } from "../setupTests";
import Job from "./Job";
import JobsService, { IJob } from "../services/JobsService";
import FilteringService from "../services/FilteringService";

describe("Job", () => {
    let component:RenderResult;
    let job:IJob;
    
    let renderJob = (i:IJob, isActive = false) => {
        let jobRef = {
            ...i,
            ref: React.createRef()
        }
        let filteringService = new FilteringService();
        let jobsService = new JobsService(filteringService, [job]);

        return render(<Job isActive={isActive} job={jobRef} jobsService={jobsService} />);
    };

    beforeEach(() => {
        job = makeJob("Company 1", ["skill1", "skill2", "skill3"], new Date(2018, 0, 1).getTime(), new Date(2019, 0, 1).getTime());
    });

    it("should have the job title", () => {
        component = renderJob(job);

        expect(component.queryByText("Company 1-jobTitle")).toBeInTheDocument();
    });

    it("should have the description", () => {
        component = renderJob(job);

        expect(job.description.length).toEqual(2);
        
        for(let i = 0; i < job.description.length; i++) {
            expect(component.queryByText(job.description[i])).toBeInTheDocument();
        }
    });

    it("should have the dates with an end date if there is one", () => {
        component = renderJob(job);

        expect(component.queryByText("Jan 2018 - Jan 2019")).toBeInTheDocument();
    });

    it("should have the dates with 'present' if there's no end date", () => {
        job.endDate = undefined;
        
        component = renderJob(job);

        expect(component.queryByText("Jan 2018 - Present")).toBeInTheDocument();
    });

    it("should have a thumbnail, if there is one", () => {
        component = renderJob(job);

        expect(component.queryByAltText("Company 1")).toBeInTheDocument();
    });

    it("should not have a thumbnail, if there is not one", () => {
        job.thumbnailUrl = undefined;

        component = renderJob(job);

        expect(component.queryByAltText("Company 1")).not.toBeInTheDocument();
    });

    it("should not have active class if not active", () => {
        component = renderJob(job);
        expect(component.container.firstChild).not.toHaveClass("active");
    });

    it("should have active class if active", () => {
        component = renderJob(job, true);
        expect(component.container.firstChild).toHaveClass("active");
    });
});
