import React from "react"
import { render, RenderResult } from "@testing-library/react"
import { IItem } from "../services/ItemsService";
import { makeItem } from "../setupTests";
import Item from "./Item";

describe("Item", () => {
    let component:RenderResult;
    let gotoUrl = jest.fn();
    let item:IItem;
    let renderItem = (i:IItem, isActive = false) => {
        let itemRef = {
            ...i,
            ref: React.createRef()
        }
        return render(<Item isActive={isActive} item={itemRef} gotoUrl={gotoUrl} />);
    };

    beforeEach(() => {
        item = makeItem("item1", ["tag1", "tag2", "tag3"]);
    });

    it("should have the item name", () => {
        component = renderItem(item);

        expect(component.queryByText("item1")).toBeInTheDocument();
    });

    it("should have the summary", () => {
        component = renderItem(item);
        
        expect(component.queryByText(item.summary)).toBeInTheDocument();
    });

    it("should have the description", () => {
        component = renderItem(item);

        expect(item.description.length).toEqual(2);
        
        for(let i = 0; i < item.description.length; i++) {
            expect(component.queryByText(item.description[i])).toBeInTheDocument();
        }
    });

    it("should have a thumbnail, if there is one", () => {
        component = renderItem(item);

        expect(component.queryByAltText("item1")).toBeInTheDocument();
    });

    it("should not have a thumbnail, if there is not one", () => {
        item.thumbnailUrl = undefined;

        component = renderItem(item);

        expect(component.queryByAltText("item1")).not.toBeInTheDocument();
    });

    it("should have a code button if there's a code url", () => {
        component = renderItem(item);

        expect(component.queryByText(/view code/i)).toBeInTheDocument();
    });

    it("should not have a code button if there is not a code url", () => {
        item.codeUrl = undefined;

        component = renderItem(item);

        expect(component.queryByText(/view code/i)).not.toBeInTheDocument();
    });

    it("should have a live button if there's a live url", () => {
        component = renderItem(item);

        expect(component.queryByText(/view live/i)).toBeInTheDocument();
    });

    it("should not have a live button if there is not a live url", () => {
        item.liveUrl = undefined;

        component = renderItem(item);

        expect(component.queryByText(/view live/i)).not.toBeInTheDocument();
    });

    it("should call the gotoUrl method when clicking the code button", () => {
        component = renderItem(item);

        component.queryByText(/view code/i)!.click();
        expect(gotoUrl).toHaveBeenCalled();
    });

    it("should call the gotoUrl method when clicking the live button", () => {
        component = renderItem(item);

        component.queryByText(/view live/i)!.click();
        expect(gotoUrl).toHaveBeenCalled();
    });

    it("should not have active class if not active", () => {
        component = renderItem(item);
        expect(component.container.firstChild).not.toHaveClass("active");
    });

    it("should have active class if active", () => {
        component = renderItem(item, true);
        expect(component.container.firstChild).toHaveClass("active");
    });
});
