import React, { MouseEvent } from "react";
import ItemsService, { IItemRef } from "../services/ItemsService";
import SkillsService, { ISkill } from "../services/SkillsService";
import Item from "./Item";
import Skill from "./Skill";
import "./App.scss";
import searchIcon from "../img/magnifying-glass.png";
import SearchMenu from "./SearchMenu";
import Nav from "./Nav";
import JobsService, { IJobRef } from "../services/JobsService";
import NavService, { INavItem, NAV_ITEMS } from "../services/NavService";
import Job from "./Job";

export interface IAppProps {
    itemsService:ItemsService;
    skillsService:SkillsService;
    jobsService:JobsService;
    navService:NavService;
}

export interface IAppState {
    skills: ISkill[];
    items: IItemRef[] | IJobRef[];
    activeItem: IItemRef | IJobRef | null;
    navItems: INavItem[];
    isMenuOpen: boolean;
}

export default class App extends React.Component<IAppProps, IAppState> {
    private isScrolling:boolean = false;
    private minActivePercentSeen = 20;
    private maxActivePercentSeen = 75;
    private searchValue:string = "";

    constructor(props:IAppProps) {
        super(props);

        let projects = this.props.itemsService.getItems();
        
        this.state = {
            skills: this.props.skillsService.skills,
            items: projects,
            activeItem: projects[0],
            isMenuOpen: false,
            navItems: this.props.navService.getNavItems()
        };

        this.onScroll = this.onScroll.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.onSearchChange = this.onSearchChange.bind(this);
        this.handleNav = this.handleNav.bind(this);
    }

    public componentDidMount() {
        window.addEventListener("scroll", this.onScroll);
        window.addEventListener("mousewheel", this.onScroll);
    }

    public componentWillUnmount() {
        window.removeEventListener("scroll", this.onScroll);
        window.removeEventListener("mousewheel", this.onScroll);
    }

    private toggleMenu() {
        this.setState({
            isMenuOpen: !this.state.isMenuOpen
        });
    }

    private onSearchChange(value: string) {
        this.searchValue = value;
        let items = this.props.navService.getActiveItem().key === NAV_ITEMS.PROJECTS ? this.props.itemsService.getItems(value) : this.props.jobsService.getJobs(value);

        this.setState({
            items
        }, () => {
            this.onScroll();
        });
    }

    private onScroll() {
        setTimeout(() => {
            if(this.isScrolling) {
                return;
            }

            this.isScrolling = true;

            let scrollingElement = document.scrollingElement || document.documentElement,
                scrollTop = scrollingElement.scrollTop,
                activeItem = this.figureActiveItem(scrollTop);

            if(activeItem) {
                this.setState({
                    activeItem
                });
            }
            else {
                this.setState({
                    activeItem: null
                });
            }
            this.isScrolling = false;
        }, 100);
    }

    private figureActiveItem(scrollTop:number) {
        let viewportHeight = window.innerHeight;

        let seenItem = this.state.items[0];

        if(scrollTop <= 0) {
            return seenItem;
        }

        // the first item whose percent seen is > 0 and < 75
        for(let i = 0; i < this.state.items.length; i++) {
            let ref = this.state.items[i].ref.current,
                elementOffsetTop = ref.offsetTop,
                elementHeight = ref.offsetHeight,
                distance = scrollTop + viewportHeight - elementOffsetTop,
                percentSeen = Math.round(distance / ((viewportHeight + elementHeight) / 100));

            if(percentSeen > this.minActivePercentSeen && percentSeen <= this.maxActivePercentSeen) {
                seenItem = this.state.items[i];
            }
        }

        return seenItem;
    }

    private gotoUrl(url:string) {
        window.open(url, "_blank");
    }

    private renderSkills() {
        return this.state.skills.map((skill) => {
            return (
                <Skill skill={skill} rating={this.props.skillsService.getRating(skill)} key={skill.name} isActive={this.props.itemsService.isSkillActive(this.state.activeItem, skill.name)} />
            );
        });
    }

    private isItemActive(item:IItemRef | IJobRef) {
        return item === this.state.activeItem;
    }

    private renderItems() {
        if(this.props.navService.getActiveItem().key === NAV_ITEMS.JOBS) {
            return (this.state.items as IJobRef[]).map((job) => {
                return (
                    // key is composite because i've worked at the same place a few times
                    <Job jobsService={this.props.jobsService} isActive={this.isItemActive(job)} job={job} key={job.companyName + job.startDate} />
                );
            });
        }

        return (this.state.items as IItemRef[]).map((item) => {
            return (
                <Item gotoUrl={this.gotoUrl} isActive={this.isItemActive(item)} item={item} key={item.name} />
            );
        });
    }

    private getStyle() {
        if(!this.state.activeItem) {
            return {};
        }

        return {
            backgroundColor: this.state.activeItem.themeColor
        };
    }

    private renderSearchMenu() {
        return (
            <SearchMenu onSearchChange={this.onSearchChange} isMenuOpen={this.state.isMenuOpen} />
        );
    }

    private handleNav(navItem:INavItem) {
        
        // kludge because this was handled after the fact.  could definitely be more elegant.
        if(navItem.key === NAV_ITEMS.RESUME) {
            window.open("http://www.jonathanadamski.com/resume.pdf");
            return;
        }

        this.props.navService.setActiveItem(navItem.key);
        this.setState({
            navItems: this.props.navService.getNavItems(),
            items: this.props.navService.getActiveItem().key === NAV_ITEMS.JOBS ? this.props.jobsService.getJobs(this.searchValue) : this.props.itemsService.getItems(this.searchValue)
        }, () => {
            this.onScroll();
        });
    }

    public render() {
        return (
            <div className="container-fluid app" style={this.getStyle()}>
                <header className="row">
                    <div className="col">
                        <h1>Jonathan Adamski</h1>
                    </div>
                    <div className="col search-icon-container">
                        <img src={searchIcon} className="search-icon" alt="Search" onClick={this.toggleMenu} />
                    </div>
                </header>
                {this.renderSearchMenu()}
                <Nav onItemClick={this.handleNav} items={this.state.navItems} />
                <div className="row">
                    <div className="col skills-container">
                        <div className="skills-container-content">
                            {this.renderSkills()}
                        </div>
                    </div>
                    <div className="col items-container">
                        {this.renderItems()}
                    </div>
                </div>
            </div>
        );
    }
}
