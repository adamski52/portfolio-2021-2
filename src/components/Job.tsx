import React from "react";
import JobsService, { IJobRef } from "../services/JobsService";
import "./Job.scss";

export interface IJobProps {
    job: IJobRef;
    isActive: boolean;
    jobsService: JobsService;
}

export interface IJobState {
    job: IJobRef;
    isActive: boolean;
}

export default class Job extends React.Component<IJobProps, IJobState> {
    constructor(props:IJobProps) {
        super(props);

        this.state = {
            ...props
        };
    }

    private renderDescription() {
        return this.state.job.description.map((text, index) => {
            return (
                <p key={index}>{text}</p>
            );
        });
    }

    private renderThumbnail() {
        if(!this.state.job.thumbnailUrl) {
            return null;
        }

        return (
            <div className="job-thumbnail" style={this.getStyle()}>
                <img src={this.state.job.thumbnailUrl} alt={this.state.job.companyName} />
            </div>
        );
    }

    private getClassName() {
        let classes = ["job"];

        if(this.state.isActive) {
            classes.push("active");
        }

        return classes.join(" ");
    }

    private getStyle() {
        return {
            backgroundColor: this.state.job.themeColor
        };
    }

    public render() {
        return (
            <div className={this.getClassName()} ref={this.state.job.ref}>
                <h2 className="job-title">{this.state.job.jobTitle}</h2>
                {this.renderThumbnail()}
                {/* <div className="row job-info"> */}
                    {/* <h3 className="col-sm-12 col-md-6 job-company-name">{this.state.job.companyName}</h3> */}
                    {/* <h4 className="col-sm-12 col-md-6 job-dates">{this.props.jobsService.getJobDateDisplay(this.state.job)}</h4> */}
                    <h4 className="job-dates">{this.props.jobsService.getJobDateDisplay(this.state.job)}</h4>
                {/* </div> */}
                <div className="job-description">
                    {this.renderDescription()}
                </div>
            </div>
        );
    }

    public static getDerivedStateFromProps(props:IJobProps, state:IJobState) {
        return {
            ...props
        };
    }
}