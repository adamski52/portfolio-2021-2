import React from "react"
import { render, fireEvent, RenderResult } from "@testing-library/react"
import SearchMenu from "./SearchMenu";

describe("SearchMenu", () => {
    let component:RenderResult;
    let onSearchChange = jest.fn();

    it("should render", () => {
        component = render(<SearchMenu onSearchChange={onSearchChange} isMenuOpen={false} />);
    });

    it("should not have open class if not open", () => {
        component = render(<SearchMenu onSearchChange={onSearchChange} isMenuOpen={false} />);
        expect(component.container.firstChild).not.toHaveClass("open");
    });

    it("should have open class if open", () => {
        component = render(<SearchMenu onSearchChange={onSearchChange} isMenuOpen={true} />);
        expect(component.container.firstChild).toHaveClass("open");
    });

    it("should have textbox for searching", () => {
        component = render(<SearchMenu onSearchChange={onSearchChange} isMenuOpen={false} />);
        expect(component.getByPlaceholderText(/filter/i)).toBeInTheDocument();
    });

    it("should call change handler on change", () => {
        component = render(<SearchMenu onSearchChange={onSearchChange} isMenuOpen={false} />);
        let textbox = component.getByPlaceholderText(/filter/i);
        fireEvent.change(textbox, {
            target: {
                value: "lol"
            }
        });

        expect(onSearchChange).toHaveBeenCalledWith("lol");
    });
});
