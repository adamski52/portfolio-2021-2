import { render, RenderResult } from "@testing-library/react"
import App from "./App";
import { makeItem, makeJob } from "../setupTests";
import ItemsService from "../services/ItemsService";
import SkillsService, { ISkill } from "../services/SkillsService";
import FilteringService from "../services/FilteringService";
import NavService from "../services/NavService";
import JobsService from "../services/JobsService";

describe("App", () => {
    let component:RenderResult;

    const item1 = makeItem("item1", ["tag1", "tag2", "tag3"]);
    const item2 = makeItem("item3", ["tag1", "tag2", "tag3"]);
    const item3 = makeItem("item2", ["tag1", "tag2", "tag3"]);

    const skill1:ISkill = {
        name: "skill1",
        skill: 0
    };

    const skill2:ISkill = {
        name: "skill2",
        skill: 50
    };
    
    const skill3:ISkill = {
        name: "skill3",
        skill: 100
    };

    const job1 = makeJob("Company 1", [skill1.name, skill2.name, skill3.name], new Date(2018, 0, 1).getTime(), new Date(2019, 0, 1).getTime());
    const job2 = makeJob("Company 2", [skill1.name, skill2.name, skill3.name], new Date(2019, 0, 1).getTime(), new Date(2020, 0, 1).getTime());
    const job3 = makeJob("Company 3", [skill1.name, skill2.name, skill3.name], new Date(2020, 0, 1).getTime(), undefined);

    const filteringService = new FilteringService();
    const itemsService = new ItemsService(filteringService, [item1, item2, item3]);
    const jobsService = new JobsService(filteringService, [job1, job2, job3]);
    const skillsService = new SkillsService([skill1, skill2, skill3]);
    const navService = new NavService();

    beforeEach(() => {
        component = render(<App itemsService={itemsService} skillsService={skillsService} jobsService={jobsService} navService={navService} />);
    });

    it("should render", () => {
        let buttons = component.getAllByText(/view code/i);
        expect(buttons.length).toEqual(3);
    });

    it("should have a header", () => {
        expect(component.queryByText(/jonathan adamski/i)).toBeInTheDocument();
    });

    it("should have a skill element for each skill", () => {
        let skill1Element = component.queryByAltText(item1.name),
            skill2Element = component.queryByAltText(item2.name),
            skill3Element = component.queryByAltText(item3.name);

        expect(skill1Element).toBeInTheDocument();
        expect(skill2Element).toBeInTheDocument();
        expect(skill3Element).toBeInTheDocument();
    });

    it("should have an item element for each item", () => {        
        let item1Element = component.queryByAltText(item1.name),
            item2Element = component.queryByAltText(item2.name),
            item3Element = component.queryByAltText(item3.name);

        expect(item1Element).toBeInTheDocument();
        expect(item2Element).toBeInTheDocument();
        expect(item3Element).toBeInTheDocument();
    });
});
