import React, { RefObject } from "react";
import ITaggedItem from "../interfaces/TaggedItem";
import FilteringService from "./FilteringService";

export interface IItem extends ITaggedItem {
    name: string;
    summary: string;
    description: string[];
    codeUrl?: string;
    liveUrl?: string;
    thumbnailUrl?: string;
    awardsUrls?: {
        where: string,
        url: string
    }[];
    themeColor: string;
}

export interface IItemRef extends IItem {
    ref: RefObject<any>;
}

export default class ItemsService {
    private items:IItemRef[];
    private matchThreshold = .6;
    private filteringService:FilteringService;

    constructor(filteringService:FilteringService, items:IItem[]) {
        this.items = items.map((item) => {
            return {
                ...item,
                ref: React.createRef()
            };
        });

        this.filteringService = filteringService;
    }

    public getItems(filter: string = "") {
        if (!filter || filter.length <= 3) {
            return this.items;
        }

        filter = filter.toLowerCase();

        return this.items.filter((item) => {
            let itemName = item.name.toLowerCase();
            if(itemName.indexOf(filter) > -1 || this.filteringService.getSimilarity(itemName, filter) > this.matchThreshold) {
                return true;
            }

            for (let i = 0; i < item.tags.length; i++) {
                let tag = item.tags[i].toLowerCase();
                if (tag.indexOf(filter) > -1 || this.filteringService.getSimilarity(tag, filter) > this.matchThreshold) {
                    return true;
                }
            }
            return false;
        });
    }

    public get allTags() {
        let tags:string[][] = [];
        this.items.forEach((item) => {
            tags.push(item.tags);
        });

        return tags;
    }

    public isSkillActive(item:ITaggedItem | null, skillName:string) {
        if(!item) {
            return false;
        }

        return item.tags.indexOf(skillName.toLowerCase()) > -1;
    }
}