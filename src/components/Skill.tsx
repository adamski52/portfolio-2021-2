import React from "react";
import { ISkill } from "../services/SkillsService";
import "./Skill.scss";

export interface ISkillProps {
    skill: ISkill;
    isActive: boolean;
    rating: string;
}

export interface ISkillState extends ISkillProps {

}

export default class Skill extends React.Component<ISkillProps, ISkillState> {
    constructor(props:ISkillProps) {
        super(props);

        this.state = {
            ...props
        };
    }

    private getClassName() {
        let classes = ["skill"];
        if(this.state.isActive) {
            classes.push("active");
        }

        return classes.join(" ");
    }

    public render() {
        return (
            <div className={this.getClassName()}>
                <span className="skill-tooltip">{this.state.skill.name + " (" + this.state.rating + " | " + this.state.skill.skill + "%)"}</span>
                <img className="skill-icon" alt={this.state.skill.name} src={"/img/icons/" + this.state.skill.name + ".png"} />
            </div>
        );
    }

    public static getDerivedStateFromProps(props:ISkillProps, state:ISkillState) {
        return {
            ...props
        };
    }
}