export interface ISkill {
    name: string;
    skill: number;
}

export const SKILL_RATING = {
    NOVICE: "novice",
    OK: "ok",
    GOOD: "good",
    EXCELLENT: "excellent",
    MASTERY: "mastery"
};

export default class SkillsService {
    private _skills:ISkill[] = [];

    constructor(skills:ISkill[], usages:string[][] = []) {
        
        // sort each skill according to it's number of usages
        this._skills = skills.map((skill) => {
            return {
                ...skill,
                usages: this.getUsages(skill, usages)
            };
        }).sort((lhs, rhs) => {
            return rhs.usages - lhs.usages;
        }).map((skill) => {
            return {
                name: skill.name,
                skill: skill.skill
            };
        });
    }

    private getUsages(skill:ISkill, usages:string[][]) {
        let num = 0;

        usages.forEach((usage) => {
            if(usage.indexOf(skill.name) > -1) {
                num++;
            }
        });

        return num;
    }

    public get skills():ISkill[] {
        return this._skills;
    }

    public getRating(skill:ISkill) {
        if(skill.skill < 60) {
            return SKILL_RATING.NOVICE;
        }

        if(skill.skill < 70) {
            return SKILL_RATING.OK;
        }

        if(skill.skill < 80) {
            return SKILL_RATING.GOOD
        }

        if(skill.skill < 90) {
            return SKILL_RATING.EXCELLENT;
        }

        return SKILL_RATING.MASTERY;
    }
}