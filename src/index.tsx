import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import "./index.scss";
import FilteringService from "./services/FilteringService";
import HttpService from "./services/HttpService";
import ItemsService from "./services/ItemsService";
import JobsService from "./services/JobsService";
import NavService from "./services/NavService";
import SkillsService from "./services/SkillsService";

(async () => {
  const httpService = new HttpService("");
  const filteringService = new FilteringService();
  const jobsService = new JobsService(filteringService, await httpService.get("/api/jobs.json"));
  const itemsService = new ItemsService(filteringService, await httpService.get("/api/items.json"));
  const skillsService = new SkillsService(await httpService.get("/api/skills.json"), itemsService.allTags);
  const navService = new NavService();

  ReactDOM.render(
    <React.StrictMode>
      <App itemsService={itemsService} skillsService={skillsService} jobsService={jobsService} navService={navService} />
    </React.StrictMode>,
    document.getElementById("root")
  );
})();
