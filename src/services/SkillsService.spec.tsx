import React from "react";
import SkillsService, { ISkill, SKILL_RATING } from "./SkillsService";

describe("SkillsService", () => {
    const skill1:ISkill = {
        name: "skill1",
        skill: 0
    };

    const skill2:ISkill = {
        name: "skill2",
        skill: 50
    };
    
    const skill3:ISkill = {
        name: "skill3",
        skill: 100
    };

    const skillsService = new SkillsService([skill1, skill2, skill3]);
    it("should gimme skills", () => {
        expect(skillsService.skills.length).toEqual(3);
    });

    it("should give 'novice' based on skill", () => {
        for(let i = 0; i < 60; i++) {
            expect(skillsService.getRating({
                name: "whatever",
                skill: i
            })).toEqual(SKILL_RATING.NOVICE);
        }
    });

    it("should give 'ok' based on skill", () => {
        for(let i = 60; i < 70; i++) {
            expect(skillsService.getRating({
                name: "whatever",
                skill: i
            })).toEqual(SKILL_RATING.OK);
        }
    });

    it("should give 'good' based on skill", () => {
        for(let i = 70; i < 80; i++) {
            expect(skillsService.getRating({
                name: "whatever",
                skill: i
            })).toEqual(SKILL_RATING.GOOD);
        }
    });

    it("should give 'excellent' based on skill", () => {
        for(let i = 80; i < 90; i++) {
            expect(skillsService.getRating({
                name: "whatever",
                skill: i
            })).toEqual(SKILL_RATING.EXCELLENT);
        }
    });

    it("should give 'mastery' based on skill", () => {
        for(let i = 90; i <= 100; i++) {
            expect(skillsService.getRating({
                name: "whatever",
                skill: i
            })).toEqual(SKILL_RATING.MASTERY);
        }
    });
});