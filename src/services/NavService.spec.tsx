import NavService, { NAV_ITEMS } from "./NavService";

describe("NavService", () => {
    const navService = new NavService();
    it("should gimme nav items", () => {
        expect(navService.getNavItems()).toEqual([{
            isActive: true,
            key: NAV_ITEMS.PROJECTS,
            text: "Projects"
        }, {
            isActive: false,
            key: NAV_ITEMS.JOBS,
            text: "History"
        }, {
            isActive: false,
            key: NAV_ITEMS.RESUME,
            text: "Resume"
        }]);
    });

    it("should set/gimme the active item", () => {
        navService.setActiveItem(NAV_ITEMS.PROJECTS);
        expect(navService.getActiveItem()).toEqual({
            isActive: true,
            key: NAV_ITEMS.PROJECTS,
            text: "Projects"
        });

        navService.setActiveItem(NAV_ITEMS.JOBS);
        expect(navService.getActiveItem()).toEqual({
            isActive: true,
            key: NAV_ITEMS.JOBS,
            text: "History"
        });

        navService.setActiveItem(NAV_ITEMS.RESUME);
        expect(navService.getActiveItem()).toEqual({
            isActive: true,
            key: NAV_ITEMS.RESUME,
            text: "Resume"
        });
    });
});