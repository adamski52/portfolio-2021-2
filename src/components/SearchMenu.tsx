import React, { ChangeEvent } from "react";
import "./SearchMenu.scss";

export interface ISearchMenuProps {
    isMenuOpen: boolean;
    onSearchChange: (value:string) => void;
}

export interface ISearchMenuState {
    isMenuOpen: boolean;
}

export default class SearchMenu extends React.Component<ISearchMenuProps, ISearchMenuState> {
    constructor(props:ISearchMenuProps) {
        super(props);

        this.state = {
            isMenuOpen: props.isMenuOpen
        };

        this.onSearchChange = this.onSearchChange.bind(this);
    }
    
    private onSearchChange(e:ChangeEvent<HTMLInputElement>) {
        this.props.onSearchChange(e.currentTarget.value);
    }

    private getClassName() {
        let classes = ["search-menu"];
        if(this.state.isMenuOpen) {
            classes.push("open");
        }

        return classes.join(" ");
    }

    public render() {
        return (
            <div className={this.getClassName()}>
                <input type="text" placeholder="filter" onChange={this.onSearchChange} className="search-menu-input" />
            </div>
        );
    }

    public static getDerivedStateFromProps(props:ISearchMenuProps, state:ISearchMenuState) {
        return {
            ...state,
            isMenuOpen: props.isMenuOpen
        };
    }
}