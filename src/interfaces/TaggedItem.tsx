export default interface ITaggedItem {
    tags: string[];
}