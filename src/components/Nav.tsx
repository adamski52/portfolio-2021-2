import React from "react";
import { INavItem } from "../services/NavService";
import "./Nav.scss";

export interface INavProps {
    onItemClick: (navItem: INavItem) => void;
    items: INavItem[];
}

export interface INavState {
    items: INavItem[];
}

export default class Nav extends React.Component<INavProps, INavState> {

    constructor(props:INavProps) {
        super(props);

        this.state = {
            items: props.items
        };
    }

    private getClassName(item:INavItem) {
        let classes = ["btn nav-item"];

        if(item.isActive) {
            classes.push("active");
        }

        return classes.join(" ");
    }

    private renderItems() {
        return this.state.items.map((item) => {
            return (
                <button key={item.key} onClick={(e) => {
                    this.props.onItemClick(item);
                }} className={this.getClassName(item)}>{item.text}</button>
            );
        });
    }

    public render() {
        return (
            <nav className="row">
                <div className="col">
                    {this.renderItems()}
                </div>
            </nav>
        );
    }

    public static getDerivedStateFromProps(props: INavProps, state: INavState) {
        return {
            ...state,
            items: props.items
        };
    }
}
